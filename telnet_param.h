/*
 * Copyright (c) 2018 Lars Thrane A/S
 * SPDX-License-Identifier: GPL-2.0
 *
 * Telnet parameters
 */
#ifndef TELNET_PARAM_H
#define TELNET_PARAM_H


/*
 * Telnet commands
 */

#define TNC_F0_SE   (0xf0)      /* End of subnegotiation parameters */
#define TNC_F1_NOP  (0xf1)      /* No operation                     */
#define TNC_F2_MARK (0xf2)      /* The data stream portion of a Synch.
                                   This should always be accompanied
                                   by a TCP Urgent notification. */
#define TNC_F3_BRK  (0xf3)      /* NVT character BRK.               */
#define TNC_F4_IP   (0xf4)      /* The function IP (interrupt process) */
#define TNC_F5_AO   (0xf5)      /* The function AO (abort output)   */
#define TNC_F6_AYT  (0xf6)      /* The function AYT (are you there) */
#define TNC_F7_EC   (0xf7)      /* The function EC (erase charceter) */
#define TNC_F8_EL   (0xf8)      /* The function EL (erase line)     */
#define TNC_F9_GA   (0xf9)      /* The GA signal (go ahead)         */
#define TNC_FA_SB   (0xfa)      /* Subnegotiation begin             */
#define TNC_FB_WILL (0xfb)      /* Indicates the desire to begin
                                   performing, or confirmation that
                                   you are now performing, the
                                   indicated option. */
#define TNC_FC_WONT (0xfc)      /* Indicates the refusal to perform,
                                   or continue performing, the
                                   indicated option. */
#define TNC_FD_DO   (0xfd)      /* Indicates the request that the
                                   other party perform, or
                                   confirmation that you are expecting
                                   the other party to perform, the
                                   indicated option. */
#define TNC_FE_DONT (0xfe)      /* Indicates the demand that the
                                   other party stop performing,
                                   or confirmation that you are no
                                   longer expecting the other party
                                   to perform, the indicated option. */
#define TNC_FF_IAC  (0xff)      /* Interpret as Command / data byte */


/*
 * Telnet options
 */

/*
 * https://www.iana.org/assignments/telnet-options/telnet-options.xhtml
 *
 * 0 	Binary Transmission					[RFC856]
 * 1 	Echo								[RFC857]
 * 2 	Reconnection						[DDN Protocol Handbook, ... ]
 * 3 	Suppress Go Ahead					[RFC858]
 * 4 	Approx Message Size Negotiation		["The Ethernet, ... ]
 * 5 	Status								[RFC859]
 * 6 	Timing Mark							[RFC860]
 * 7 	Remote Controlled Trans and Echo	[RFC726]
 * 8 	Output Line Width					[DDN Protocol Handbook, ... ]
 * 9 	Output Page Size					[DDN Protocol Handbook, ... ]
 * 10 	Output Carriage-Return Disposition	[RFC652]
 * 11 	Output Horizontal Tab Stops			[RFC653]
 * 12 	Output Horizontal Tab Disposition	[RFC654]
 * 13 	Output Formfeed Disposition			[RFC655]
 * 14 	Output Vertical Tabstops			[RFC656]
 * 15 	Output Vertical Tab Disposition		[RFC657]
 * 16 	Output Linefeed Disposition			[RFC658]
 * 17 	Extended ASCII						[RFC698]
 * 18 	Logout								[RFC727]
 * 19 	Byte Macro							[RFC735]
 * 20 	Data Entry Terminal					[RFC1043][RFC732]
 * 21 	SUPDUP								[RFC736][RFC734]
 * 22 	SUPDUP Output						[RFC749]
 * 23 	Send Location						[RFC779]
 * 24 	Terminal Type						[RFC1091]
 * 25 	End of Record						[RFC885]
 * 26 	TACACS User Identification			[RFC927]
 * 27 	Output Marking						[RFC933]
 * 28 	Terminal Location Number			[RFC946]
 * 29 	Telnet 3270 Regime					[RFC1041]
 * 30 	X.3 PAD								[RFC1053]
 * 31 	Negotiate About Window Size			[RFC1073]
 * 32 	Terminal Speed						[RFC1079]
 * 33 	Remote Flow Control					[RFC1372]
 * 34 	Linemode							[RFC1184]
 * 35 	X Display Location					[RFC1096]
 * 36 	Environment Option					[RFC1408]
 * 37 	Authentication Option				[RFC2941]
 * 38 	Encryption Option					[RFC2946]
 * 39 	New Environment Option				[RFC1572]
 * 40 	TN3270E								[RFC2355]
 * 41 	XAUTH								[Rob_Earhart]
 * 42 	CHARSET								[RFC2066]
 * 43 	Telnet Remote Serial Port (RSP)		[Robert_Barnes]
 * 44 	Com Port Control Option				[RFC2217]
 * 45 	Telnet Suppress Local Echo			[Wirt_Atmar]
 * 46 	Telnet Start TLS					[Michael_Boe]
 * 47 	KERMIT								[RFC2840]
 * 48 	SEND-URL							[David_Croft]
 * 49 	FORWARD_X							[Jeffrey_Altman]
 * 50-137 	Unassigned						[IANA]
 * 138 	TELOPT PRAGMA LOGON					[Steve_McGregory]
 * 139 	TELOPT SSPI LOGON					[Steve_McGregory]
 * 140 	TELOPT PRAGMA HEARTBEAT				[Steve_McGregory]
 * 141-254 	Unassigned
 * 255 	Extended-Options-List				[RFC861]
*/

#define TNO_BINARY   0          /* Binary Transmission          */
#define TNO_ECHO     1          /* Echo                         */
#define TNO_SUPP_GA  3          /* Suppress Go Ahead            */
#define TNO_LOGOUT  18          /* Logout                       */
#define TNO_TTYP    24          /* Terminal Type                */
#define TNO_NAWS    31          /* Negotiate About Window Size  */
#define TNO_NEO     39          /* New Environment Option       */
#define TNO_CPCO    44          /* RFC 2217                     */
#define TNO_KERMIT  47          /* Kermit                       */

#endif /* TELNET_PARAM_H */
